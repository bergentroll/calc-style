#!/usr/bin/env python

'''
Script makes numbers sequence like printed by lcd-calculator.
First type integer size or just press enter (by default size = 1),
then type some numeric sequence.
'''

import os


def get_size(size):
    '''Returns width and height incuding "floor" and "ceil"'''
    try:
        size = int(size)
    except ValueError:
        size = 1
    return(size + 2, 2 * size + 5)


def make_dicto(width, height):
    '''Make dicto of ASCII-signs according to size value'''
    l = ['---', ' - ', '   ', '| |', '|  ', '  |']
    dicto = {}
    for i in range(len(l)):    # width
        l[i] = l[i][0] + l[i][1] * (width - 2) + l[i][2]
    dicto = {
        '0': [l[0], l[1], l[3], l[2], l[3], l[1], l[0]],
        '1': [l[0], l[2], l[5], l[2], l[5], l[2], l[0]],
        '2': [l[0], l[1], l[5], l[1], l[4], l[1], l[0]],
        '3': [l[0], l[1], l[5], l[1], l[5], l[1], l[0]],
        '4': [l[0], l[2], l[3], l[1], l[5], l[2], l[0]],
        '5': [l[0], l[1], l[4], l[1], l[5], l[1], l[0]],
        '6': [l[0], l[1], l[4], l[1], l[3], l[1], l[0]],
        '7': [l[0], l[1], l[5], l[2], l[5], l[2], l[0]],
        '8': [l[0], l[1], l[3], l[1], l[3], l[1], l[0]],
        '9': [l[0], l[1], l[3], l[1], l[5], l[1], l[0]],
        ' ': ['-', ' ', ' ', ' ', ' ', ' ', '-'],
        '|': ['x', '|', '|', '|', '|', '|', 'x']
    }
    for i in dicto.keys():    # height
        d_height = height - len(dicto[i])
        grow_point_1 = dicto[i][2]
        grow_point_2 = dicto[i][4]
        for j in range(d_height // 2):
            dicto[i].insert(len(dicto[i]) // 2, grow_point_1)
            dicto[i].insert(len(dicto[i]) // 2 + 1, grow_point_2)
    return(dicto)


def format(line):
    '''add spaces and "walls"'''
    output_line = '|'
    for i in range(len(line) - 1):
        output_line += line[i] + ' '
    output_line += line[len(line) - 1] + '|'
    return(output_line)


def decorate(line):
    '''makes beauty ASCII-signs'''
    for level in range(height):
        output_line = ''
        for num in line:
            output_line += dicto[num][level]
        yield output_line


def get_term_width():
    try:
        width = int(os.popen('tput cols').read())
        if width:
            return(int(width))
    except Exception:
        pass
    try:
        terminal_size = os.get_terminal_size()
        return(terminal_size.columns)
    except Exception:
        return(80)


def convert(line_i):
    '''convert line to calc-style and slice for term width'''
    term_width = get_term_width()
    output = [line for line in list(decorate(format(line_i)))]
    if len(output[0]) <= term_width:
        draw(output)
    else:
        while len(output[0]) > term_width:
            output_slice = [slice[0:term_width] for slice in output]
            draw(output_slice)
            output = [slice[term_width:] for slice in output]
        draw(output)


def draw(output):
    '''print joined strings'''
    print('\n'.join([line for line in output]))

width, height = get_size(input('Enter size (integer, 1 by default): '))
dicto = make_dicto(width, height)
convert(input('Enter sequence of digits: '))
